# README #

Firing_range2: demo mission for a target pop up and scoring script/tool I am working on. Features advanced scoring of targets, full AI compatibility (AI can be given a drill and will attempt to complete it ad best as possible) this is great for creating ambience on a server our on a mission to give the impression other soldiers are practicing their shooting. Score can veer accessed easily, and is uploaded to database for persistent score board.

Latest version available https://bitbucket.org/Tirpitz/arma-missions/raw/master/latest.zip


I am passively looking for collaborators to build a more extensive framework for mission builders. 
I am working on a "hardcore" survival mode, with sun/shade detection, digging for water, dehydration, shelter etc if anyone is interested.

### Who do I talk to? ###

* Myself: Tirpitz