# Installation #



## Basic ##

Copy the `Tirp` folder to the mission root directory.

Add the following code to `Description.ext`:

`class CfgFunctions
{
	#include "Tirp\functions.h"
};`
Or if you already have a `class CfgFunctions` add this to the existing class.

`#include "Tirp\functions.h"`

## Enabling Individual modules ##
Most modules can be enabled independently.

To enable a module edit the `Tirp\functions.h` file. commenting/uncommenting the appropriate line. 

It is requested that the Core module is left enabled and it is required by several modules as it supplies a required function library.

### Option 1: permanently activate resupply ###


Add the following to the `init.sqf`

```
//enable resupply by default:

missionNamespace setVariable ["TIRP_resupply_enabled",True, True];
```

If this method is chosen it is recommended to also increase the number of resupplies permitted using:

```
//change the number of resupplies per group to 4:

missionNamespace setVariable ["TIRP_resupply_max_supplies", 4, True];
```

### Option 2: Zeus/admin controlled resupply ###

Link at least one unit with a game master module or place at least one module with #adminlogged or other means of control.

For basic installation this is it.

Central supply location

 
If you would like all resupply crates to arrive at a central location place a marker with the variable name `TIRP_resupply` on the map where you would like the supplies to arrive.

Group/Squad specific locations
 
Place markers for each squad or group that requires a custom location. Use the following naming convention: `TIRP_resupply_<callsign>` for example `TIRP_resupply_Bravo 1-2`.

Note 1: that spaces are allowed ass markers are not required to follow variable naming rules.

Note 2: it is recommended that you also place a central depot location in case a group name changes for whatever reason during the mission that it does not fail unexpectedly (if this does happen the box will simply be delivered straight to the requesting player).

