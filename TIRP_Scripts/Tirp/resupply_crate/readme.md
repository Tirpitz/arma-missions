# Tirp -TAW Resupply script

Author: Lönja "Tirpitz" Selter

# Requirements

Arma 3

Text editor (install only)

# Installation

## Basic

Copy the `Tirp` folder to the mission root directory.

Add the following code to `Description.ext`:

`class CfgFunctions

{

#include "Tirp\functions.h"

};`

Or if you already have a `class CfgFunctions` add this to the existing class.

`#include "Tirp\functions.h"`

### Option 1: permanently activate resupply

Add the following to the `init.sqf`
```
//enable resupply by default:

missionNamespace setVariable ["TIRP_resupply_enabled",True, True];
```
If this method is chosen it is recommended to also increase the number of resupplies permitted using:
```
//change the number of resupplies per group to 4:

missionNamespace setVariable ["TIRP_resupply_max_supplies", 4, True];

```
### Option 2: Zeus/admin controlled resupply

Link at least one unit with a game master module or place at least one module with #adminlogged or other means of control.

For basic installation this is it.

## Central supply location

![](img1.png)

If you would like all resupply crates to arrive at a central location place a marker with the variable name `TIRP_resupply` on the map where you would like the supplies to arrive.

## Group/Squad specific locations

![](img2.png)

Place markers for each squad or group that requires a custom location. Use the following naming convention: `TIRP_resupply_\&lt;callsign\&gt;` for example `TIRP_resupply_Bravo 1-2`.

Note 1: that spaces are allowed ass markers are not required to follow variable naming rules.

Note 2: it is recommended that you also place a central depot location in case a group name changes for whatever reason during the mission that it does not fail unexpectedly (if this does happen the box will simply be delivered straight to the requesting player).

# Usage

## Players

![](img3.png)

Players can access the supply script using the diary on the map screen under Resupply \&gt; Player.

The options available are:

- Check
  - Checks whether the resupply is enabled
- Request
  - Requests a resupply to the configured location.
  - Location is determined my mission maker as follows if a squad specific marker exists use it, else try the central marker, otherwise fall back to the player location.
- Enable Admin options
  - Enables the admin options under Resupply \&gt; Curator. See below

### Setup

Players request a resupply box fill it with their desired equipment. Then using the scroll menu save it. This saves the boxes content to their profile. This can be done at the beginning of the mission or on any other server/mission.

Once the resupply is required the player simply requests the supply from the diary.

Resupplies are limited by group/squad so should be used sparingly. This can be adjusted by the mission maker or mission admins.

## Curator/Admin

![](img4.png)

Curators can access the supply script using the diary on the map screen under Resupply \&gt; Curator once `Open admin options` has been used from the player menu (see above).

The options available are:

- Enable
  - Enables the spawning of resupply boxes.
- Disable
  - Disables the spawning of resupply boxes but does not remove existing boxes from play.
- Check
  - Checks the current status of the resupply script.
- Enable Modification
  - Allows players to update their saved loadouts.
- Disable Modification
  - Prevents users from updating their loadouts (useful during a mission to prevent accidental overwriting of a large loadout).
- Check Modification
  - Checks the status of loadout modification.
- Reset Supplies
  - Resets the number of supplies called for everyone to 0.

# Customisation:

This can be added to the mission init.sqf to modify behaviour. These commands can also be executed via the admin console or other triggers or events and should function as expected.

## Ammo crate:
```

//change the ammo crate class: replace "Box_NATO_Ammo_F" with class of your choice

missionNamespace setVariable ["TIRP_resupply_crate_class","Box_NATO_Ammo_F", True];


```
## Enable resupply by default
```
//enable modification by default:

missionNamespace setVariable ["TIRP_resupply_mod_enabled",True, True];

```
## Change number of boxes available per squad (default 1)
```
//change the number of resupplies per group to 4:


missionNamespace setVariable ["TIRP_resupply_max_supplies", 4, True];

```
## Enable changing of the loadout by default
```
//enable loadout modification

missionNamespace setVariable [""TIRP_resupply_mod_enabled"",True, True]

```
## Allow saving of other crates as resupply loadout

Add this action to the desired crate:
```

_crate addAction ["Save Resupply Crate", {_this call tirp_fnc_resupplySaveCrate;}];

```
