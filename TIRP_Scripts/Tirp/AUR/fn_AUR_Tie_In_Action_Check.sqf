if (player getVariable ["AUR_Is_Rappelling", false]) exitWith {False;};
_ropes = GetPosATL player nearObjects ["Rope", 2];
// systemChat str count _ropes;
_ret = False;
scopeName "TIRP_TieInCheck";

{
_ret = _x getVariable ["TIRP_AUR_isRappelRope", False];
	if (_ret) then {breakTo "TIRP_TieInCheck";}
	} forEach _ropes;
if (_ret and "AUR_BelayDevice" in items player ) then {
_ret = true;
}else {_ret = false;};
_ret;
