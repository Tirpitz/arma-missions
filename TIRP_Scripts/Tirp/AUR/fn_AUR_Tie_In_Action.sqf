//todo tie in action from rappel function
_ropes = GetPosATL player nearObjects ["Rope", 2];
// systemChat str count _ropes;
_ret = False;
scopeName "TIRP_RopeFinder";
_rope = "";
{
_ret = _x getVariable ["TIRP_AUR_isRappelRope", False];
if (_ret) then {_rope = _x;  breakTo "TIRP_RopeFinder";}
} forEach _ropes;

_length = ropeLength _rope;
//clean up old rope
_rope setVariable ["TIRP_AUR_InUse", True, True];
_oldAnchor = ( _rope getVariable "TIRP_AUR_RopeAnchor");
_oldWeight = ( _rope getVariable "TIRP_AUR_RopeWeight");
_oldAnchor2 = ( _rope getVariable "TIRP_AUR_RopeWeight2"); // dummy rope anchor 1
_oldAnchor3 = ( _rope getVariable "TIRP_AUR_RopeWeight3"); //dummy rope anchor 2
ropeDestroy _rope;

// systemChat str ((ropeEndPosition _rope) select 0);

_anchor = createVehicle ["Land_Can_V2_F", player, [], 0, "CAN_COLLIDE"];
systemChat str _anchor;
systemChat str _length;
_dist = player distance _anchor;
_topRopeStartLength = _dist;

// Create rappel device (attached to player)
_rappelDevice = createVehicle ["B_static_AA_F", player, [], 0, "CAN_COLLIDE"];
// hideObject _rappelDevice;
_rappelDevice setPosWorld (position player);
_rappelDevice allowDamage false;
// [[_rappelDevice],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;

[[player,_rappelDevice,_anchor],"TIRP_fnc_AUR_Play_Rappelling_Sounds_Global"] call TIRP_fnc_AUR_RemoteExecServer;

_bottomRopeStartLength = _length - (_dist +1);

_ropeBottom = ropeCreate [_rappelDevice, [-0.15,0,0], _bottomRopeStartLength];
_ropeBottom allowDamage false;
_ropeTop = ropeCreate [_rappelDevice, [0,0.15,0], _anchor, [0, 0, 0], _dist +1];
_ropeTop allowDamage false;
systemChat str ( ropeAttachedObjects  _anchor); 
systemChat str _ropeTop;
systemChat str _ropeBottom;