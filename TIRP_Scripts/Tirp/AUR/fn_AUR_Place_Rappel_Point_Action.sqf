params ["_player","_ropeLength"];
_oldRope = _player getVariable "TIRP_AUR_Rope";
ropeDestroy _oldRope;
deleteVehicle (_player getVariable "TIRP_AUR_RopeEndObject");
systemChat str _ropeLength;
_dummyAnchor = _player getVariable "TIRP_AUR_AnchorObject";
// hideObject _anchor;
// _anchor enableSimulation false;
// _anchor allowDamage false;
// [[_anchor],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;

// Create anchor for rope (at rappel point)
_dummyAnchor2 = createVehicle ["Land_Can_V2_F", _player, [], 0, "CAN_COLLIDE"];
hideObject _dummyAnchor2;
_dummyAnchor2 enableSimulation false;
_dummyAnchor2 allowDamage false;
[[_dummyAnchor2],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;
// Create anchor for rope (at rappel point)
_anchor3 = createVehicle ["Land_Can_V2_F", _player, [], 0, "CAN_COLLIDE"];
hideObject _anchor3;
_anchor3 enableSimulation false;
_anchor3 allowDamage false;
[[_anchor3],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;
hint typeName _rappelPoint;
_ropeAnchor = "";

hint "creating dummy rope";
_anchor = createVehicle ["B_static_AA_F", _player, [], 0, "CAN_COLLIDE"];

// hideObject _dummyAnchor;
[[_anchor],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;
_ropeAnchor = ropeCreate[_dummyAnchor, [0,0,-0.95],_dummyAnchor2, [0,0,0]];
_ropeAnchor allowDamage false;
systemChat str _ropeAnchor;
_ropeAnchor setVariable ["TIRP_AUR_isAnchorRope", true, true];
_anchor setVariable ["TIRP_AUR_isAnchor", True, true];
// 

_dummyAnchor addAction ["This is the dummy Anchor", {}];


_anchorRopeLength = ropeLength _ropeAnchor;
systemChat str _anchorRopeLength;
// // Create rappel device (attached to player)
// _rappelDevice = createVehicle ["B_static_AA_F", _player, [], 0, "CAN_COLLIDE"];
// hideObject _rappelDevice;
// _rappelDevice setPosWorld _playerStartPosition;
// _rappelDevice allowDamage false;
// [[_rappelDevice],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;

// [[_player,_rappelDevice,_anchor],"TIRP_fnc_AUR_Play_Rappelling_Sounds_Global"] call TIRP_fnc_AUR_RemoteExecServer;

_bottomRopeStartLength = _ropeLength - 1;
_topRopeStartLength = _ropeLength - _anchorRopeLength;
systemChat str _topRopeStartLength;
// if(_playerStartingOnGround) then {
	// _topRopeStartLength = abs ((_rappelPoint select 2) - (getPosASL _player select 2) - 3);
	// _bottomRopeStartLength = (_ropeLength - _topRopeStartLength) max 2;
// };

// _ropeBottom = ropeCreate [_rappelDevice, [-0.15,0,0], _bottomRopeStartLength];
// _ropeBottom allowDamage false;

_weight = createVehicle ["Land_Can_V2_F", _player, [], 0, "CAN_COLLIDE"];
// hideObject _weight;
// [[_weight],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;
_ropeTop = ropeCreate [_anchor, [0, 0, -0.95], _weight, [0,0,0], 60 ];
_weight attachTo [_player,[0,0,0],"lefthand"];
// hideObject _anchor;
// _anchor enableSimulation false;
// _anchor allowDamage false;
detach _weight;
_weight setVelocity ((getCameraViewDirection _player) vectorMultiply 3);

_ropeTop allowDamage false;
systemChat str ropeLength _ropeTop;
systemChat str _ropeTop;
_ropeTop setVariable ["TIRP_AUR_isRappelRope", true, true];
_ropeTop setVariable ["TIRP_AUR_RopeAnchor", _anchor, true];
_ropeTop setVariable ["TIRP_AUR_RopeWeight", _weight, true];
_ropeTop setVariable ["TIRP_AUR_RopeAnchor2", _dummyAnchor, true];
_ropeTop setVariable ["TIRP_AUR_RopeAnchor3", _dummyAnchor2, true];
player setVariable ["TIRP_AUR_IsHoldingRope", false, true];