params ["_player","_rappelPoint","_rappelDirection","_ropeLength"];
//hint format ["Player: %1 \nPre_rappel_position: %2, \nRappelPoint: %3 \n RappelDirection: %4,\n RopeLength: %5.", str _player,str  _playerPreRappelPosition,str _rappelPoint, str _rappelDirection,str  _ropeLength];
// _player setVariable ["AUR_Is_Rappelling",true,true];

systemChat "Attaching rope...";

anchor = createVehicle ["B_static_AA_F", _rappelPoint, [], 0, "CAN_COLLIDE"];

anchor2 = createVehicle ["Land_Can_V2_F", player, [], 0, "CAN_COLLIDE"];
rope = ropeCreate [anchor, [0,0,-1.5],anchor2, [0,0,0],_ropeLength];
// hideObject anchor;
// anchor enableSimulation false;
// anchor allowDamage false;
[[anchor],"TIRP_fnc_AUR_Hide_Object_Global"] call TIRP_fnc_AUR_RemoteExecServer;
rope allowDamage false;
anchor2 attachTo [player,[0,0,0],"lefthand"];
systemChat "settign vars";
_player setVariable ["TIRP_AUR_IsHoldingRope", true, true];
_player setVariable ["TIRP_AUR_RopeEndObject", anchor2, true];
_player setVariable ["TIRP_AUR_AnchorObject", anchor, true];
_player setVariable ["TIRP_AUR_Rope", rope, true];

