params ["_uuid", "_group", "_fps", "_fps_min", "_serverTime"];
//systemChat "Write...";
if (!isServer) exitWith {};
_formatString = missionNamespace getVariable ["TIRP_perfmon_string","TIRP_perfmon_entry: Server Time: %5 UUID: %1, Group: %2, FPS: %3, FPS_min: %4"];
diag_log (format [_formatString, _uuid, _group, _fps, _fps_min, _serverTime]);