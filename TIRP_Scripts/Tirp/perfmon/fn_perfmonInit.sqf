
tirp_perfmonHandle = [] spawn {
	systemChat "TIRP perfmon initialized";
	while {true} do {
		sleep (missionNamespace getVariable ["TIRP_perfmon_interval", 60]);
		if (missionNamespace getVariable ["TIRP_perfmon_enabled", true]) then {
			[] spawn tirp_fnc_perfmonClient;
			if (isServer) then {
				logEntities;
			};
		};
	}
};

estimatedTimeLeft  7200 ;