waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "statusBar" call BIS_fnc_rscLayer;
//_rscLayer cutRsc["statusBar","PLAIN"];
systemChat "Wilkommen Bei EG Altis Life";

[] spawn {
	sleep 5;
	_statusText = "Welcome";
	_counter = 180;
	_timeSinceLastUpdate = 0;
	while {true} do
	{
		sleep 1;
		_counter = _counter - 1;
		_allPlayers = playableUnits;
		_bluforAlive = ({(side _x )== west} count _allPlayers);
		_civAlive = ({(side _x )== civilian} count _allPlayers);
		_indieAlive = ({(side _x )== independent} count _allPlayers);
		_opforAlive = ({(side _x )== east} count _allPlayers);
		_statusText = "web.EliteGamingClan.de";
		_secondsT = serverTime;
		_hours = floor(_secondsT/3600);
		_secondR1 = (_secondsT - (_hours *3600));
		_minutes = floor (_secondR1/60);
		_secondR2 =round ( _secondR1 - (_minutes *60));
		_timeStr = ( str(_hours ) +":"+ str(_minutes) +":" + str(_secondR2));

		((uiNamespace getVariable "statusBar")displayCtrl 1000)ctrlSetStructuredText  parseText format["%3 | TS3 : ts.elitegamingclan.de | <t color='#ff0000'>Cash: %8</t> | <t color='#8888FF'>Server Uptime: %9 </t>|<t color='#8888FF'> FPS: %1 </t>| Lebendige Spieler : %2 |<t color='#0000FF'> Cops : %5 </t>|<t color='#881166'> Civs : %6 </t>| <t color='#00FF00'>Med : %7 </t>   ", round diag_fps, count playableUnits, _statusText, _counter, _bluforAlive, _civAlive, _indieAlive, life_cash, _timeStr];
	};
};
