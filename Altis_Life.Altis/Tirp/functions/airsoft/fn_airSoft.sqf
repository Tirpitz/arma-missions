/*
    File: fn_airSoft.sqf
    Author: Skalicon modded by Mahribar
    
    Description: Original Script for rubber bullets Modified for Airsoft
*/
private["_unit","_shooter","_curWep","_curMags","_attach","_val"];
airSoftTimeout = 10;
diag_log "airsoft initiated";

if (round(airsoftTimeOutTime - time) >= 0 ) exitWith {};
airsoftTimeOutTime = time + airSoftTimeout;

_val = 1000; // prize/cost of hit
_unit = [_this,0,Objnull,[Objnull]] call BIS_fnc_param;


if (typeName (_this select 0) == "STRING" )then {
		hint "you started playing airsoft";


	_selectSpawnPoint = round(random(6));
			switch (_selectSpawnPoint) do
			{
				case 0: {
					player setPos (getMarkerPos "airsoft_salt_spawn1");
				};
				case 1: {
					player setPos (getMarkerPos "airsoft_salt_spawn2");
				};
				case 2: {
					player setPos (getMarkerPos "airsoft_salt_spawn3");
				};
				case 3: {
					player setPos (getMarkerPos "airsoft_salt_spawn4");
				};
				case 4: {
					player setPos (getMarkerPos "airsoft_salt_spawn5");
				};
				case 5: {
					player setPos (getMarkerPos "airsoft_salt_spawn6");
				};
				case 6: {
					player setPos (getMarkerPos "airsoft_salt_spawn7");
				};
			};
			
			exit;
} else {};
_shooter = [_this,1,Objnull,[Objnull]] call BIS_fnc_param;
if(isNull _unit OR isNull _shooter) exitWith {player allowDamage true; life_isdowned = false;};

if(_shooter isKindOf "Man" && alive player  && ((([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_castle") < 150) ||(([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_salt") < 150) || (([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_pyrgos") < 80) )) then
	{	//got shot in airsoft area
	if (([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_salt") < 150) then 
		{
		 //player is in airsoft salt area
			player setPos (getMarkerPos "airsoft_castle");
			hint localize "STR_Airsoft_hit","PLAIN";
			player allowDamage true;

			// Telo: Teleport
			_selectSpawnPoint = round(random(6));
			switch (_selectSpawnPoint) do
			{
				case 0: {
					player setPos (getMarkerPos "airsoft_salt_spawn1");
				};
				case 1: {
					player setPos (getMarkerPos "airsoft_salt_spawn2");
				};
				case 2: {
					player setPos (getMarkerPos "airsoft_salt_spawn3");
				};
				case 3: {
					player setPos (getMarkerPos "airsoft_salt_spawn4");
				};
				case 4: {
					player setPos (getMarkerPos "airsoft_salt_spawn5");
				};
				case 5: {
					player setPos (getMarkerPos "airsoft_salt_spawn6");
				};
				case 6: {
					player setPos (getMarkerPos "airsoft_salt_spawn7");
				};
			};

			[_unit, tirp_airsoftCost, ["STR_NOTF_Airsoft_hit_payout", "STR_NOTF_Airsoft_hit_payup"]] call tirp_fnc_moneyTransfer;
		
		}; 
		if (([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_pyrgos") < 80) then 
		{
		 //player is in airsoft pyrgos area
			//player setPos (getMarkerPos "airsoft_pyrgos_exit");
			hint localize "STR_Airsoft_hit","PLAIN";
			player allowDamage true;

			// Telo: Teleport
			_selectSpawnPoint = group player;
			
			_grp1 = group airsoft_pyrgos_shop_1;
			_grp2 = group airsoft_pyrgos_shop_2;
			switch (_selectSpawnPoint) do
			{
				case (_grp1): {
					player setPos (getMarkerPos "airsoft_pyrgos_spawn_1");
				};
				case (_grp2): {
					player setPos (getMarkerPos "airsoft_pyrgos_spawn_2");
				};
			};

			[_unit, tirp_airsoftCost, ["STR_NOTF_Airsoft_hit_payout", "STR_NOTF_Airsoft_hit_payup"]] call tirp_fnc_moneyTransfer;
		
		}; 
		if (([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_castle") < 150) then   {
		
			player setPos (getMarkerPos "airsoft_castle");
			hint localize "STR_Airsoft_hit","PLAIN";
			player allowDamage true;

			
			
				//max span distance from marker
			_spawnRandomisation=150; //.15km
			_spwnposNew = [(getMarkerPos "airsoft_castle"),random _spawnRandomisation, random 360] call BIS_fnc_relPos;

			_pos = [1,1,1];


			_isSafe = 0;
		while { _isSafe == 0 } 
				do 
				{


			_spwnposNew = [(getMarkerPos "airsoft_castle"),random _spawnRandomisation, random 360] call BIS_fnc_relPos;
			_pos = _spwnposNew;

		_isSafe = count (_pos isFlatEmpty [1,5,2,0,1,false,player]) ;

			};
		(vehicle player) setpos [_pos select 0, _pos select 1, 0];
		//diag_log "safe position found";
			[_unit, tirp_airsoftCost, ["STR_NOTF_Airsoft_hit_payout", "STR_NOTF_Airsoft_hit_payup"]] call tirp_fnc_moneyTransfer;
				};
				
				
		
			
		}
			 
		else
		{ 
		_unit allowDamage true;
		};


	