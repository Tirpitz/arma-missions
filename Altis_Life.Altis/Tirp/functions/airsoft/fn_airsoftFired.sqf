/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Handles various different ammo types being fired.
*/
private["_ammoType","_projectile"];
_ammoType = _this select 4; 
_projectile = _this select 6;
_weapon = _this select 1;
_magazine = _this select 5;

if (_weapon =="srifle_LRR_F" || _weapon == "srifle_LRR_LRPS_F" || _weapon =="srifle_LRR_SOS_F") then {
	player setAmmo [currentWeapon player, 0];
};