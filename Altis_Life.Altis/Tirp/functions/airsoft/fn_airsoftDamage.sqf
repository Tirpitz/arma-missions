//default damage
#include <macro.h>
private["_unit","_damage","_source","_projectile","_part","_curWep"];
_unit = SEL(_this,0);
_part = SEL(_this,1);
_damage = SEL(_this,2);
_source = SEL(_this,3);
_projectile = SEL(_this,4);
 

//Airsoft

if(!isNull _source) then {
    if(_source != _unit) then {
        _curMag = currentMagazine _source;
		_curWep = currentWeapon _source;
        if (_curWep in 
		["arifle_TRG20_F","arifle_TRG20_ACO_F","arifle_TRG20_ACO_Flash_F","arifle_TRG20_ACO_pointer_F","arifle_TRG20_Holo_F","arifle_TRG20_ARCO_pointer_F","hgun_Rook40_snds_F","hgun_Rook40_F","srifle_LRR_LRPS_F","srifle_LRR_SOS_F"]
		) 
		then {
            private["_isVehicle","_isQuad"];
                _isVehicle = if(vehicle player != player) then {true} else {false};
                _isQuad = if(_isVehicle) then {if(typeOf(vehicle player) == "B_Quadbike_01_F") then {true} else {false}} else {false};
                _damage = false;    
                
                if(_isVehicle || _isQuad) then {
                    player action ["Eject",vehicle player];
                    [_unit,_source] spawn  tirp_fnc_airSoft;
                } else {
                    [_unit,_source] spawn tirp_fnc_airSoft;
                };
				_damage = false;
			
        };
    };
};
if ((([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_castle") < 150 ) && _projectile == "") then
{
	_damage = false;
};
if ((([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_salt") < 150 ) && _projectile == "") then
{
	_damage = false;
};
if ((([position player select 0,position player select 1,0] distance getmarkerpos "airsoft_pyrgos") < 100 ) && _projectile == "") then
{
	_damage = false;
};

diag_log "airsoft damage calculated";
_damage;