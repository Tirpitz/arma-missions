#include <macro.h>

/*****************************************************************
	File: fn_tirpTransferMoney.sqf
	Created By: Lonja "Tirpitz" Selter
	Based on file fn_bankTransfer.sqf By  Bryan "Tonic" Boardwine
	transfers money from player a to player b.
	parameters: 
		[playerA(player object),playerB(player object),amount (int),cash(bool)]
******************************************************************/
private ["_playerA","_playerB", "_amount","_playerAInitialMoney"];

_amount = _this select 0;

_unit = _this select 1;
_messages = _this select 2;

_messages = [_this,2,["STR_ATM_SentMoneySuccess", "STR_NOTF_money_Recieve"],[[]]] call BIS_fnc_param;
if(isNull _unit) exitWith {};
if((lbCurSel 2703) == -1) exitWith {hint localize "STR_ATM_NoneSelected"};
if(isNil "_unit") exitWith {hint localize "STR_ATM_DoesntExist"};
if(_val > 999999) exitWith {hint localize "STR_ATM_TransferMax";};
if(_val < 0) exitwith {};
if(!([str(_val)] call life_fnc_isnumeric)) exitWith {hint localize "STR_ATM_notnumeric"};
if(_val > BANK) exitWith {hint localize (_messages select 0)};
_tax = [_val] call life_fnc_taxRate;
if((_val + _tax) > BANK) exitWith {hint format[localize "STR_ATM_SentMoneyFail",_val,_tax]};

BANK = BANK - (_val + _tax);

[[_val,profileName,(_messages select 1)],"tirp_fnc_moneyRecieve",_unit,false,true] call life_fnc_MP;
[] call life_fnc_atmMenu;
hint format[localize _messages select 0,[_val] call life_fnc_numberText,_unit getVariable["realname",name _unit],[_tax] call life_fnc_numberText];
[1] call SOCK_fnc_updatePartial;
