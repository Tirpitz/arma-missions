class WeaponShops {
    //Armory Shops
    class gun {
        name = "Billy Joe's Firearms";
        side = "civ";
        license = "gun";
        level = -1;
        msg = "";
        items[] = {
            { "hgun_Rook40_F", "", 6500 },
            { "hgun_Pistol_heavy_02_F", "", 9850 },
            { "hgun_ACPC2_F", "", 11500 },
            { "hgun_PDW2000_F", "", 20000 },
            { "optic_ACO_grn_smg", "", 2500 },
            { "V_Rangemaster_belt", "", 4900 },
            { "16Rnd_9x21_Mag", "", 25 },
            { "9Rnd_45ACP_Mag", "", 45 },
            { "6Rnd_45ACP_Cylinder", "", 50 },
            { "30Rnd_9x21_Mag", "", 75 }
        };
    };
    class rebel {
        name = "Mohammed's Jihadi Shop";
        side = "civ";
        license = "rebel";
        level = -1;
        msg = "";
        items[] = {
            {"arifle_TRG21_F","",30000},
			{"arifle_Katiba_F","",40000},
			{"srifle_DMR_01_F","",50000},
			{"arifle_SDAR_F","",20000},
			{"srifle_EBR_F","",325000},
			{"srifle_GM6_F","",2650000},
			{"LMG_Zafir_F","",500000},
			{"hgun_ACPC2_F","",20000},
			{"30Rnd_45ACP_Mag_SMG_01","",50000},
			{"launch_RPG32_F","",2500000},
			{"RPG32_F","",2250000},
			{"optic_ACO_grn","",3500},
			{"optic_Holosight","",3600},
			{"optic_Aco","",500},
			{"optic_SOS","",25000},
			{"optic_MRCO","",2500},
			{"optic_DMS","",5500},
			{"optic_MRD","",500},
			{"optic_LRPS","",25000},
			{"optic_NVS","",150000},
			{"optic_tws","",250000},
			{"acc_flashlight","",1000},
			{"optic_Hamr","",7500},
			{"muzzle_snds_M","",12750},
			{"muzzle_snds_B","",100000},
			{"30Rnd_9x21_Mag","",200},
			{"20Rnd_556x45_UW_mag","",125},
			{"10Rnd_762x51_Mag","",500},
			{"30Rnd_65x39_caseless_green","",275},
			{"20Rnd_762x51_Mag","",8500},
			{"5Rnd_127x108_Mag","",20000},
			{"5Rnd_127x108_APDS_Mag","",30000},
			{"150Rnd_762x51_Box_Tracer","",100000},
			{"30Rnd_556x45_Stanag","",1000},
			{"Laserdesignator","",10000},
			{"Laserbatteries","",1000},
			{"ItemRadio","Cellphone",500}
        };
    };
    class gang {
        name = "Hideout Armament";
        side = "civ";
        license = "";
        level = -1;
        msg = "";
        items[] = {
            { "hgun_Rook40_F", "", 1500 },
            { "hgun_Pistol_heavy_02_F", "", 2500 },
            { "hgun_ACPC2_F", "", 4500 },
            { "hgun_PDW2000_F", "", 9500 },
            { "optic_ACO_grn_smg", "", 950 },
            { "V_Rangemaster_belt", "", 1900 },
            { "16Rnd_9x21_Mag", "", 25 },
            { "9Rnd_45ACP_Mag", "", 45 },
            { "6Rnd_45ACP_Cylinder", "", 50 },
            { "30Rnd_9x21_Mag", "", 75 }
        };
    };
    //Basic Shops
    class genstore {
        name = "Altis General Store";
        side = "civ";
        license = "";
        level = -1;
        msg = "";
        items[] = {
        	{ "Binocular", "", 150 },
			{ "ItemGPS", "", 100 },
			{ "ToolKit", "", 250 },
			{ "FirstAidKit", "", 150 },
			{ "NVGoggles", "", 2000 },
			{ "Chemlight_red", "", 300 },
			{ "Chemlight_yellow", "", 300 },
			{ "Chemlight_green", "", 300 },
			{ "Chemlight_blue", "", 300 }
        };
    };
	 class airsoft {
        name = "Ultimate Airsoft";
        side = "civ";
        license = "";
        level = -1;
        msg = "";
        items[] = {
        	{"arifle_TRG20_F","Airsoft Assault Rifle",2500},
			{"30Rnd_556x45_Stanag_Tracer_Red","airsoft mag (tracer: red)",3000},
			{"30Rnd_556x45_Stanag_Tracer_Green","airsoft mag (tracer: green)",3000},
			{"30Rnd_556x45_Stanag_Tracer_Yellow","airsoft mag (tracer: yellow)",3000},
			{"hgun_Rook40_snds_F","Airsoft Handgun silent",2000},
			{"hgun_Rook40_F","Airsoft Handgun",1500},
			{"16Rnd_9x21_Mag","airsoft mag Pistol",500},
			{"30Rnd_9x21_Mag","airsoft mag Pistol +",2500},
			{"srifle_LRR_LRPS_F","Airsoft Sniper",6000},
			{"7Rnd_408_Mag","Airsoft Sniper Single Shot",800},
			{"SmokeShellBlue","Smoke Blue",1000},
			{"SmokeShellRed","Smoke Red",1000},
			{"SmokeShellGreen","Smoke Green",1000},
			{"SmokeShellPurple","Smoke Purple",1000},
			{"ItemGPS","",100},
			{"FirstAidKit","",150},
			{"NVGoggles","",2000}
        };
    };
    //Cop Shops
    class cop_basic {
        name = "Altis Cop Shop";
        side = "cop";
        license = "";
        level = -1;
        msg = "";
        items[] = {
        	{ "arifle_sdar_F", "Taser Rifle", 20000 },
			{ "hgun_P07_snds_F", "Stun Pistol", 2000 },
			{ "hgun_P07_F", "", 7500 },
			{ "HandGrenade_Stone", "Flashbang", 1700 },
			{ "Binocular", "", 150 },
			{ "ItemGPS", "", 100 },
			{ "ToolKit", "", 250 },
			{ "muzzle_snds_L", "", 650 },
			{ "FirstAidKit", "", 150 },
			{ "Medikit", "", 1000 },
			{ "NVGoggles", "", 2000 },
			{ "16Rnd_9x21_Mag", "", 50 },
			{ "20Rnd_556x45_UW_mag", "Taser Rifle Magazine", 125 }
        };
    };
    class cop_patrol {
        name = "Altis Patrol Officer Shop";
        side = "cop";
        license = "";
        level = 2;
        msg = "You must be a Patrol Officer Rank!";
        items[] = {
        	{ "arifle_MX_BlackF", "", 35000 },
			{ "SMG_02_ACO_F", "", 30000 },
			{ "HandGrenade_Stone", "Flashbang", 1700 },
			{ "MineDetector", "", 1000 },
			{ "acc_flashlight", "", 750 },
			{ "optic_Holosight", "", 1200 },
			{ "optic_Arco", "", 2500 },
			{ "muzzle_snds_H", "", 2750 },
			{ "30Rnd_65x39_caseless_mag", "", 130 },
			{ "30Rnd_9x21_Mag", "", 250 }
        };
    };
    class cop_sergeant {
        name = "Altis Sergeant Officer Shop";
        side = "cop";
        license = "";
        level = 3;
        msg = "You must be a Sergeant Rank!";
        items[] = {
            { "SMG_02_ACO_F", "", 15000 },
			{ "hgun_ACPC2_F", "", 17500 },
			{ "HandGrenade_Stone", "Flashbang", 1700 },
			{ "arifle_MXC_F", "", 30000 },
			{ "optic_Arco", "", 2500 },
			{ "muzzle_snds_H", "", 2750 },
			{ "30Rnd_65x39_caseless_mag", "", 100 },
			{ "30Rnd_9x21_Mag", "", 60 },
			{ "9Rnd_45ACP_Mag", "", 200 }
        };
    };
	class cop_swat {
        name = "Altis Sergeant Officer Shop";
        side = "cop";
        license = "";
        level = 5;
        msg = "Sie sind Kein SEK Beamter";
        items[] = {
			{"arifle_MX_F","",35000},
			{"arifle_MX_SW_F","",65000},
			{"arifle_MXM_F","",95000},
			{"arifle_MX_Black_F","",35000},
			{"srifle_EBR_F","",125000},
			{"hgun_Pistol_heavy_01_F","",12500},
			{"optic_Arco","",2500},
			{"optic_Hamr","",2500},
			{"optic_Aco","",500},
			{"optic_MRCO","",2500},
			{"muzzle_snds_H","",2750},
			{"optic_NVS","",150000},
			{"muzzle_snds_H","",2750},
			{"muzzle_snds_acp","",750},
			{"acc_flashlight","",2750},
			{"acc_pointer_IR","",2750},
			{"muzzle_snds_B","",12750},
			{"30Rnd_65x39_caseless_mag","",170},
			{"100Rnd_65x39_caseless_mag","",700},
			{"20Rnd_762x51_Mag","",1000},
			{"11Rnd_45ACP_Mag","",1000},
			{"Laserdesignator","",10000},
			{"Laserbatteries","",1000},
			{"HandGrenade_Stone","Flashbang",1700},
			{"SmokeShellYellow","Tear Gas",100},
			{"B_UAV_01_F","",50000},
			{"B_UavTerminal","",500}
        };
    };
	class cop_swat2 {
        name = "Altis SEK Beamter Shop 2";
        side = "cop";
        license = "";
        level = 7;
        msg = "Sie sind Kein Operations Leiter";
        items[] = {
			{"arifle_MX_Black_F","",35000},
			{"arifle_MX_SW_Black_F","",65000},
			{"arifle_MXM_Black_F","",95000},
			{"arifle_MX_Black_F","",35000},
			{"srifle_EBR_F","",125000},
			{"srifle_GM6_F","",295000},
			{"hgun_Pistol_heavy_01_F","",12500},
			{"optic_Arco","",2500},
			{"optic_Hamr","",2500},
			{"optic_Aco","",500},
			{"optic_SOS","",25000},
			{"optic_MRCO","",2500},
			{"optic_DMS","",5500},
			{"optic_MRD","",500},
			{"optic_LRPS","",25000},
			{"optic_NVS","",150000},
			{"optic_tws","",250000},
			{"muzzle_snds_H","",2750},
			{"muzzle_snds_acp","",750},
			{"acc_flashlight","",2750},
			{"acc_pointer_IR","",2750},
			{"muzzle_snds_B","",12750},
			{"muzzle_snds_H_MG","",15750},
			{"30Rnd_65x39_caseless_mag","",170},
			{"100Rnd_65x39_caseless_mag","",700},
			{"20Rnd_762x51_Mag","",1000},
			{"5Rnd_127x108_Mag","",10000},
			{"5Rnd_127x108_APDS_Mag","",15000},
			{"200Rnd_65x39_cased_Box_Tracer","",5000},
			{"150Rnd_762x51_Box_Tracer","",5000},
			{"11Rnd_45ACP_Mag","",1000},
			{"HandGrenade_Stone","Flashbang",1700},
			{"SmokeShellYellow","Tear Gas",100}
        };
    };
    //Medic Shops
    class med_basic {
        name = "store";
        side = "med";
        license = "";
        level = -1;
        items[] = {
            { "ItemGPS", "", 100 },
			{ "Binocular", "", 150 },
			{ "ToolKit", "", 250 },
			{ "FirstAidKit", "", 150 },
			{ "Medikit", "", 500 },
			{ "NVGoggles", "", 1200 },
			{ "B_FieldPack_ocamo", "", 3000 }
        };
    };
};
