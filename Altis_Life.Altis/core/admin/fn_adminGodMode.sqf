#include <macro.h>
/*
	File: fn_adminGodMode.sqf
	Author: Tobias 'Xetoxyc' Sittenauer
 
	Description: Enables God mode for Admin
*/

if(FETCH_CONST(life_adminlevel) < 3) exitWith {closeDialog 0; hint localize "STR_ANOTF_ErrorLevel";};

[] spawn {
  while {dialog} do {
   closeDialog 0;
   sleep 0.01;
  };
};
 
if(life_god) then {
	life_god = false;
	titleText ["God mode disabled","PLAIN"]; titleFadeOut 2;
	//[format ["%1 has disabled God Mode: ",(name player), (str _pos)],"tirp_fnc_chatSpam",true, false,false] call BIS_fnc_MP;
	[[0, (format ["%1 has disabled God Mode: ",(name player), (str _pos)]), false], "life_fnc_broadcast",true] call life_fnc_MP;
	player allowDamage true;
} else {
	life_god = true;
	titleText ["God mode enabled","PLAIN"]; titleFadeOut 2;
	player allowDamage false;
	//[format ["%1 has enabled God Mode: ",(name player), (str _pos)],"tirp_fnc_chatSpam",true, false,false] call BIS_fnc_MP;
	[[0, (format ["%1 has enabled God Mode: ",(name player), (str _pos)]), false], "life_fnc_broadcast",true] call life_fnc_MP;
};