/***********************************************************
File: fn_isGully.sqf


Author: Lönja "Tirpitz" Selter
Status: Initial testing

Description: detects whether player is in a gully (determines chance to find water at location) the more positive the steeper the gully. 
	a gully in this case is defined by two points Xm either side of the player being an average of Ym higher than the ground under the player; and none of these points are more than Zm lower than the player

Load:
************************************************************/
private ["_tirpUnit","_tirpUnitPos","_tirpAngles","_tirpTestDist","_tirpGully"];


_tirpUnit = player; // debug
_tirpUnitPos = (_this select 0);
_tirpAngles = [0,45,90,135]; //

_tirpTestDist = 10;
_tirpGully = 0;
for "_x" from 0 to (count _tirpAngles -1)do 
	{
	
	_windTestHeightDeltaArr=[];
	_currentElement = _tirpAngles select _x;
	_currentElement2 = _currentElement + 180;
	
		//hint str _currentElement2;
	_heightDelta = ( (ATLToASL([_tirpUnit, _tirpTestDist,_currentElement] call BIS_fnc_relPos ) select 2) - ( _tirpUnitPos select 2 ) );
	if (_heightDelta < -1 ) exitWith {_gully = -0.5 ;};
	_heightDelta2 = ( (ATLToASL([_tirpUnit, _tirpTestDist,_currentElement2] call BIS_fnc_relPos ) select 2) - ( _tirpUnitPos select 2 ) );
	if (_heightDelta2 < -1) exitWith {_gully = -0.5;};
	_windTestHeightDeltaArr set [(count _windTestHeightDeltaArr), _heightDelta];
	_windTestHeightDeltaArr set [(count _windTestHeightDeltaArr), _heightDelta2];
	//hint str _windTestHeightDeltaArr;
	_gully = (_windTestHeightDeltaArr call lib_fnc_avg);
	
	
		if  (_gully > _tirpGully) then {
		
			_tirpGully = _gully;
		};
	
	//hint str ([_tirpUnitPos, 5,_currentElement] call BIS_fnc_relPos );
	};
	
//irpAvgDelta = _windTestHeightDeltaArr call lib_fnc_avg;
//hint str _windTestHeightDeltaArr;
//_tirpAvgDelta
_tirpGully