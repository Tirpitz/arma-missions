/***********************************************************
File: fn_initTirp.sqf


Author: Lönja "Tirpitz" Selter


Description: 

Load:
************************************************************/
diag_log "******************************************************";
diag_log "*                                                    *";
diag_log '*     Survival Mission by Lonja "Tirpitz" Selter     *';
diag_log "*                   Version 0.1                      *";
diag_log "*                                                    *";
diag_log "******************************************************";

diag_log "Initialising Tirp Core.......";
tirpCoreInitComplete = false;


_handle = spawn survival_fnc_loadConfig; //load starting variables
if  (ParamsArray select 0 == 1 ) then 
	{tirp_debug = true ;} 
	else {tirp_debug = false;};
_keyInit= call survival_fnc_keyHandlerInit;


diag_log "Tirp Core Initialisation complete";
tirpCoreInitComplete = true;