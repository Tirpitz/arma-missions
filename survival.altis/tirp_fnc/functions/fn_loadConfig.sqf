/***********************************************************
File: fn_loadConfig.sqf


Author: Lönja "Tirpitz" Selter
Status: Started

Description: 

Load:
************************************************************/

switch (_this select 0) do 
	{
	case "DB": // provisional hook to load config from database instead of file.
	{};
	case "FILE":
	{
	#include <configPlayer.cfg>
	};
	};
	
	
configLoaded = true;