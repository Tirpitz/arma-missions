/***********************************************************
File: fn_survivalBloodSugar.sqf


Author: Lönja "Tirpitz" Selter
Status: Started

Description: converts food into bloodsugar/energy

Load:
************************************************************/
if !(hasInterface) exitWith {false};

diag_log "Starting Survival Food";


while (survival) do {
	waitUntil {!playerBloodSugarLock && !playerFoodLock};
	playerBloodSugarLock = true;
	playerFoodLock = true;
	_food = player getVariable ["food"];
	_bloodSugar = player getVariable ["bloodSugar"];
	if (_food >1) then {
		if _bloodSugar < 100 then {
			_food = _food - FoodToBLoodSugarRate;
			_bloodSugar = _bloodSugar + FoodToBLoodSugarRate;
			}
		}
	player setVariable ["bloodSugar", _bloodSugar]
	player setVariable ["food", _food]
	playerBloodSugarLock = false;
	playerFoodLock = false;
	
	sleep 60;
	};



