/***********************************************************
File: fn_isShade.sqf


Author: Lönja "Tirpitz" Selter

Status: Nearly Finished
Description: 
takes a positionASL and returns a bool
true if in the shade, false if in the sun. 

Load:
************************************************************/


private ["_sunPos","_unitPos", "_sunAngle","_sunElev","_intersects","_intersectsTerrain","_shade", "_ignore"];
_unitPos = _this select 0;
if( count _this >1 )then {
_ignore = _this select 1 ;
}else {_ignore = objNull};

_sunAngle = call survival_fnc_sunAngle;
_sunElev = call survival_fnc_sunElev;
_sunPos = [_unitPos, 500, _sunAngle, _sunElev] call lib_fnc_relPos2;

//_sunPos = [_sunPos select 0, _sunPos select 1, _sunPos select 2 +1];

//_unitPos = [(_unitPos select 0), (_unitPos select 1), ((_unitPos select 2 )+ 1)];

_intersects = lineIntersects [_unitPos, _sunPos, _ignore ];


_intersectsTerrain = terrainIntersectASL [ _unitPos,  _sunPos];


//player setPosASL (_unitPos);
/*
_debugCone = "RoadCone_F" createVehicle (getPos player);
_offset = player worldToModel ASLToATL _sunPos;
_debugCone attachTo [player,_offset]; 
_debugCone2 = "RoadCone_F" createVehicle (getPos player);
_offset2 = player worldToModel ASLToATL _unitPos;
_debugCone2 attachTo [player,_offset2]; 

*/
_shade = false;

if (_intersects) then {_shade = true};
if (_intersectsTerrain  ) then {_shade = true};


_shade