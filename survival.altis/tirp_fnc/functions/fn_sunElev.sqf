/***********************************************************
File: fn_sunElev.sqf


Author: Lönja "Tirpitz" Selter
Status: Initial testing

Description: returns the elevation angle of the sun.

Load:
************************************************************/

private ["_declination","_lat","_day", "_hour","_hourAngle","_elevation"];


_lat = -1 * getNumber(configFile >> "CfgWorlds" >> worldName >> "latitude");

_hour = (daytime / 24) * 360;

//hint str _hour;
_declination  = call survival_fnc_declination;
//hint str _declination;
_hourAngle = call survival_fnc_hourAngle;
_elevation = asin ((cos(_hourAngle) *cos(_declination) *cos(_lat) )+ (sin(_declination)*sin(_lat)));
//hint str _elevation;

_elevation