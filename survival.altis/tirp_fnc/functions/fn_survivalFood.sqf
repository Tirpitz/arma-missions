/***********************************************************
File: fn_survivalFood.sqf


Author: Lönja "Tirpitz" Selter
Status: Started

Description: base hunger function, based on starvation period of 30 days, other factors may reduce this time. 

this sets the base body functions energy use

2592000 seconds in 30 days reevaluated every 10 seconds

Load:
************************************************************/
if !(hasInterface) exitWith {false};

diag_log "Starting Survival Food";


while (survival) do {

	waitUntil {!playerFoodLock};
	playerFoodLock = true;
	
	player setVariable ["food",((player getVariable ["food"])-(1/2592.00))];
	
	playerFoodLock = false;
	
	
	sleep 10;
	};

