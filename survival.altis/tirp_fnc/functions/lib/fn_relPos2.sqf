/***********************************************************
File: fn_relPos2.sqf


Author: Lönja "Tirpitz" Selter
Status: Initial testing

Description: returns position xm way from unit or position and y deg from initial position and z deg abov initial position


[initialPos or unit, distance, bearing, elevation]
Load:
************************************************************/

private ["_iniPos","_distance","_bearing","_elevation","_heightDelta","_correctedDistance","_pos2d","_pos3d"];

_iniPos = [];

if ( typeName (_this select 0) == "ARRAY" )then {
		_iniPos = (_this select 0);
	} else {
	
	_iniPos = getPos (_this select 0 );

	};

//hint str _iniPos;
	

_distance = _this select 1;
_bearing = _this select 2;


_elevation = _this select 3;
_correctedDistance = _distance*cos(_elevation);

_pos2d = [_iniPos,_correctedDistance,_bearing] call BIS_fnc_relPos;


_heightDelta = _distance *sin (_elevation);

_pos3d = [_pos2d select 0, _pos2d select 1, ((_pos2d select 2) + _heightDelta)];

_pos3d