/***********************************************************
File: sum.sqf


Author: Lönja "Tirpitz" Selter


Description: sums up integers in array

Load: 
		100 elements: 0.350482 ms
************************************************************/



private ["_arr","_sum"];

_arr = _this;
_sum = 0;

	{
	
	if (typeName (_x ) == "SCALAR") then {
	_sum = _sum + ( _x);
	
	};
	
	} forEach _arr;
//hint typeName _sum;
_sum