/***********************************************************
File: fn_avg.sqf


Author: Lönja "Tirpitz" Selter


Description: calculates average for numbers in array

Load:
		3 element: 0.0359863 ms
		100 elements: 0.36241 ms
************************************************************/


private ["_arr","_sum","_avg"];

_arr = _this;

_sum = _arr call lib_fnc_sum;
_len = (count _arr);

_avg = (_sum/_len);


_avg