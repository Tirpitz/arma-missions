/***********************************************************
File: fn_survivalTemperature.sqf


Author: Lönja "Tirpitz" Selter
Status: Started

Description: 
Load:




************************************************************/
if !(hasInterface) exitWith {false};

diag_log "Starting Survival coreTemp";


while (survival) do {
	waitUntil {!playerCoreTempLock};
	_ambient = player getVariable ["ambientTemp", 20];
	playerCoreTempLock = true;
	player setVariable ["coreTemp",((player getVariable ["coreTemp"])-(0.0083333333))];
	playerCoreTempLock = false;
	sleep 10;
	};
