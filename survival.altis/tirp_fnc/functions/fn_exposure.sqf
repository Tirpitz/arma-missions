/***********************************************************
File: fn_exposure.sqf

Author: L�nja "Tirpitz" Selter
Status: Incomplete

Description: Calculates exposuire to wind

Load:
************************************************************/


private ["_tirpWindDir","_tirpWindSpeed","_windTestPointArr","_sunTestPointArr","_tirpUnit","_tirpAngles","_tirpUnitPos"];


_tirpUnit = player ;//_this select 0;

_tirpWindDir = windDir;
_tirpWindSpeed = vectorMagnitude wind;
_tirpWindSpeedFactor = 1/_tirpWindSpeed; 
_tirpAngles = [_tirpWindDir,_tirpWindDir-60,_tirpWindDir +60,_tirpWindDir-180];
_tirpUnitPos = getPosASL _tirpUnit;

hint str (_tirpUnitPos select 2);
_windTestPointArr = [];
_windTestHeightDeltaArr =[];

for "_x" from 0 to (count _tirpAngles -1 )do 
	{
	_currentElement = _tirpAngles select _x; 
	if (_currentElement >360) then {_tirpAngles set [_x, (_currentElement-360)]} 
		else {
		if (_currentElement < 0) then {_tirpAngles set [_x, (_currentElement + 360)]};
		
		};
	};

for "_x" from 0 to (count _tirpAngles -1)do 
	{
	_currentElement = _tirpAngles select _x;

	_heightDelta = ( (ATLToASL([_tirpUnit, 5,_currentElement] call BIS_fnc_relPos ) select 2) - ( _tirpUnitPos select 2 ) );
	
	_windTestHeightDeltaArr set [(count _windTestHeightDeltaArr), str _heightDelta];
	//hint str ([_tirpUnitPos, 5,_currentElement] call BIS_fnc_relPos );
	};
hint str _windTestHeightDeltaArr select 0;

_testVar = _windTestHeightDeltaArr select 0;

//hint _testVar;
//WaitUntil {!isnil "core_sunAngle"};

//[_tirpUnit, _tirpWindSpeedFactor, _tirpWindDir] call BIS_fnc_relPos;


