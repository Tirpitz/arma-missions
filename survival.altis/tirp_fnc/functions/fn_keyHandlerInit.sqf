/***********************************************************
File: fn_keyHandlerInit.sqf


Author: Lönja "Tirpitz" Selter


Description: 

Load:
************************************************************/

waitUntil {!(isNull (findDisplay 46))};
diag_log "Display 46 Found";
(findDisplay 46) displayAddEventHandler ["KeyDown", "_this call survival_fnc_keyHandler"];
hint "keyHandler added";
diag_log "keyhandler loaded";