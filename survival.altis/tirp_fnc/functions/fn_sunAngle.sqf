/************************************************************
File: fn_sunAngle.sqf
Author: Lönja "Tirpitz" Selter
Status: Initial testing
Description:
		returns angle in degrees to the sun.
		 ORIGINAL CODE BY CARLGUSTAFFA returned an innacurate value for sun elevation for improved version: see fn_sunElev
		
		so I re wrote it
***************************************************************/
private ["_lat","_day","_hour","_sunAngle"];

_lat = -1 * getNumber(configFile >> "CfgWorlds" >> worldName >> "latitude");
_day = 360 * (dateToNumber date);
_hour = (daytime / 24) * 360;
//_sunAngle = ((12 * cos(_day) - 78) * cos(_lat) * cos(_hour)) - (24 * sin(_lat) * cos(_day));  
_sunAngle = acos((sin(call survival_fnc_declination)-sin(call survival_fnc_sunElev)*sin(_lat))/(cos(call survival_fnc_sunElev)* cos(_lat)));
_sunAngle