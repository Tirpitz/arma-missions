/***********************************************************
File: fn_initSurvival.sqf


Author: Lönja "Tirpitz" Selter
Status: Started

Description: 

Load:
************************************************************/
if !(hasInterface) exitWith {false};
waitUntil {sleep 1; tirpCoreInitComplete == true};
diag_log "initialising Survial funtions";


_foodHandle = [] spawn survival_fnc_survivalFood;

_waterHandle = [] spawn survival_fnc_survivalWater;

_tempHandle = [] spawn survival_fnc_survivalTemperature;


diag_log "survival Functions initialised";