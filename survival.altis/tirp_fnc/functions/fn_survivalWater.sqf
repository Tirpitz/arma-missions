/***********************************************************
File: fn_survivalWater.sqf


Author: Lönja "Tirpitz" Selter
Status: Started

Description: based on a dehydration time of 7 days @20 degrees (tempDehydrationOffest)

604800 seconds

revaluated every 10 seconds
Load:
************************************************************/
if !(hasInterface) exitWith {false};
diag_log "Starting Survival Water";


while (survival) do {
	waitUntil {!playerWaterLock};
	playerWaterLock = true;
	player setVariable ["water",((player getVariable ["water"])-(1/604.80))];
	playerWaterLock = false;
	sleep 10;
	};


