class survival
{
	tag = "survival";
	class survival_fnc
	{
		file = "tirp_fnc\functions";
		class exposure {};
		class sunAngle {};
		class initTirp {postInit = 1;};
		class initSurvival {posInit = 1;};
		class keyHandlerInit {};
		class keyHandler {};
		class test{};
		class isGully {};
		class isShade {};
		class sunElev {};
		class hourAngle {};
		class declination {};
		class loadConfig {};
	};
	
	
};
class lib
{
	tag = "lib";
	class tirp_lib
	{
		file = "tirp_fnc\functions\lib";
		class sum {};
		class avg {};
		class relPos2 {};
		
	};
	
	
};
class debug
{
	tag = "debug";
	class debug
	{
		file = "tirp_fnc\functions\debug";
		class teleport {};

		
	};
	
	
};