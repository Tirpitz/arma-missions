/*****************************************************************
	File: fn_tirpTransferMoney.sqf
	Created By: Lonja "Tirpitz" Selter
	
	transfers money from player a to player b.
	parameters: 
		[playerA(player object),playerB(player object),amount (int),cash(bool)]
******************************************************************/
private ["_playerA","_playerB","_cash", "_amount","_playerAInitialMoney"];

//todo: build check
//check whether player has enough fn_tirpTransferMoney
//todo: call wiretransfer
//todo: build cash transfer on life_Server
