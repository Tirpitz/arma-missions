#include <macro.h>
/***********************************************
	File: fn_airsoftRecieve.sqf
	Author: Lonja "tirpitz" Selter
	based on fn_wireTransfer
	Description: Redundant file, working on new system using fn_airsftPay.sqf
 *********************************************/

/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Initiates the wire-transfer
*/
private["_value","_from"];
_value = [_this,0,0,[0]] call BIS_fnc_param;
_from = [_this,1,"",[""]] call BIS_fnc_param;
_message = [_this,2,"STR_NOTF_money_Recieve",[""]] call BIS_fnc_param;
if(EQUAL(_value,0) OR EQUAL(_from,"") OR EQUAL(_from,profileName)) exitWith {}; //No
ADD(BANK,_value);
hint format[localize _message,_from,[_value] call life_fnc_numberText];