	/******************************************************************
	File: fn_airsoftQuad.sqf

	Author: Lonja "Tirpitz" Selter
	
	description: monitor quad positions and port them back if they have been stolen.
********************************************************************/
_this spawn {

_unit = _this select 0;

while {true} do {
	if (((getMarkerPos "airSoft_Spawn") distance _unit) > 155) then
	{
	_unit setVelocity [0,0,0];
	{ _x action ["Eject", _unit]; } forEach crew _unit; 

	sleep 3;
	_spawnRandomisation=140; 
_spwnposNew = [(getMarkerPos "airSoft_Spawn"),random _spawnRandomisation, random 360] call BIS_fnc_relPos;
_pos = [1,1,1];

_isSafe = count (_pos isFlatEmpty [5,5,0.5,0,1,false,player]);

 while { _isSafe == 0 } 
		do 
		{


		_spwnposNew = [(getMarkerPos "airSoft_Spawn"),random _spawnRandomisation, random 360] call BIS_fnc_relPos;
		_pos = _spwnposNew;

		_isSafe = count (_pos isFlatEmpty [5,5,0.5,0,1,false,player]) ;

		}; 
	
_spawnPos = [_pos select 0, _pos select 1, 0];

_unit setPos _spawnPos;
} else { sleep 2};
};
};