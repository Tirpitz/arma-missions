/*
	Author: Crix
	Description:
	Search for nearby pre-built objects like trees, brushes, stone in a nearestObjects-like manner. 
	Use this function carefully as it is extremely slow.  

	Parameter(s):
	0: ARRAY - Position of search center
	1: ARRAY of STRINGS - Array of .p3d names
	2: SCALAR - Search range


	Returns:
	ARRAY - Array of objects. [] if no objects were found. 

	Example
	//--- Find nearby brushes & trees
	_flora = [position player, ["b_ficusc1s_f", "b_ficusc2d_f", "b_neriumo2d_f", "b_aroundod3s_f", "b_aroundod2s_f"], 250] call CRX_fnc_nearestP3Ds; 

*/

CRX_fnc_nearestP3Ds = {
_pos	= [_this, 0, [0,0,0], [[]], [2,3]] call BIS_fnc_param;
_types	= [_this, 1, ["All"], [[]]] call BIS_fnc_param;
_range 	= [_this, 2, 50, [0]] call BIS_fnc_param;

_objs 	= nearestObjects [_pos, [], _range]; 
_colon 	= 58; 
_dot 	= 46; 	


_allOfType	= []; 
{
	_obj = _x; 
	if (typeOf _obj isEqualTo "") then 
	{
		_arr 	= toArray str(_obj); 
		_start 	= _arr find 58; 
		if !(_start < 0) then 
		{
			_name = []; 
			_ID = [];
			_end = _arr find 46;
			_split = _arr find 58;
			for "_k" from (0) to (_split - 1) do 
			{
				_ID set [count _ID, _arr select _k]; 
			}; 
			for "_k" from (_split + 2) to (_end - 1) do 
			{
				_name set [count _name, _arr select _k]; 
			}; 
			
			
			if (toString _name in _types) then
			{
				_objArr = [ parseNumber (toString _ID), toString _name];
				_allOfType set [count _allOfType, _objArr]; 
			}; 
		}; 
	}; 
}
count _objs; 

_allOfType
};
