class tirp{
	tag = "tirp";
	class misc
		{
			file = "Tirp\functions\misc";
			class skipBrief {
				preInit = 1;
				recompile = 1;
			};

		};

	class airsoft
	{
		file = "Tirp\functions\airsoft";
		class airsoftInit{
			postInit = 1;
		};
		class airsoftQuad{};
		class airsoftPay{};
		class airsoftDamage{};
		class airsoft {};
		class airsoftFired{};
	};
	
	class money
	{
		file = "Tirp\functions\money";
		class moneyTransfer {};
		class moneyRecieve{};

	};
	class admin
	{
		file = "Tirp\functions\admin";
		class chatSpam {};

	};
};
class CRX {
	tag = "CRX";
	class misc {
		file = "Tirp\functions\misc";
		class nearestObjects{};
	}
};
class osef {
	tag = "osef";
	class misc {
		file = "Tirp\functions\osef";
		class statusBar{};
	}
};
